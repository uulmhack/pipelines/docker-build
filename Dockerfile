# This file is a template, and might need editing before it works on your project.
FROM python:3.6-alpine

# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
RUN apk --no-cache add postgresql-client

WORKDIR /usr/src/app


COPY . /usr/src/app

# For Django
# EXPOSE 8000
# CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

# For some other command
CMD ["python", "app.py"]
